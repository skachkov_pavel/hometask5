﻿using HomeTask4.Controllers;
using HomeTask4.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using Moq;
using System.Threading.Tasks;

namespace HomeTask.UnitTests
{
    [TestClass]
    public class ProjectTasksControllerTests
    {
        [TestMethod]
        public async Task SetTaskAsComplete()
        {
            var userRepoMock = new Mock<IRepository<User>>();
            var updated = new User { id = 0, firstName = "Test" };
            userRepoMock.Setup(m => m.Read(0)).ReturnsAsync(new User { id = 0, firstName = "Old" });
            var sut = new UsersController(userRepoMock.Object);

            await sut.Put(updated);
        }
    }
}
