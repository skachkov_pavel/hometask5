﻿using HomeTask4.Controllers;
using HomeTask4.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using Moq;
using System.Threading.Tasks;

namespace HomeTask.UnitTests
{
    [TestClass]
    public class UsersControllerTests
    {
        [TestMethod]
        public async Task CreateUser()
        {
            var userRepoMock = new Mock<IRepository<User>>();
            var sut = new UsersController(userRepoMock.Object);
            var user = new User { id = 0 };
            var result = await sut.Post(user);

            Assert.AreEqual(result.StatusCode, 201);
        }

        [TestMethod]
        public async Task AddUserToTeam()
        {
            var userRepoMock = new Mock<IRepository<User>>();
            var sut = new UsersController(userRepoMock.Object);
            var user = new User { id = 0, teamId = 0 };
            var result = await sut.Post(user);

            Assert.AreEqual(result.StatusCode, 201);
        }
    }
}
