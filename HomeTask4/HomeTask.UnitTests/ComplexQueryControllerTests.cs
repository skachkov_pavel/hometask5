using HomeTask4.Controllers;
using HomeTask4.Storage;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeTask.UnitTests
{
    [TestClass]
    public class ComplexQueryControllerTests
    {
        [TestMethod]
        public async Task GetUserTasksCountPerProject_HappyFlow()
        {
            var projectTaskRepoMock = new Mock<IRepository<ProjectTask>>();

            projectTaskRepoMock
                .Setup(m => m.ReadAll())
                .ReturnsAsync(new List<ProjectTask> { new ProjectTask { id = 0 } });
            var userRepoMock = new Mock<IRepository<User>>();

            var sut = new ComplexQueryController(projectTaskRepoMock.Object, userRepoMock.Object);

            var result = await sut.GetUserTasksCountPerProject(0);
            var okResult = result as OkObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult.StatusCode == 200);

            var tasks = okResult.Value as Dictionary<int, int>;
            Assert.IsTrue(tasks.Count == 1);
            Assert.AreEqual(tasks.Keys.First(), 0);
        }

        [TestMethod]
        public async Task GetUserTasksCountPerProject_NotFound()
        {
            var projectTaskRepoMock = new Mock<IRepository<ProjectTask>>();

            projectTaskRepoMock
                .Setup(m => m.ReadAll())
                .ReturnsAsync(new List<ProjectTask> { new ProjectTask { id = 0 } });
            var userRepoMock = new Mock<IRepository<User>>();

            var sut = new ComplexQueryController(projectTaskRepoMock.Object, userRepoMock.Object);

            var result = await sut.GetUserTasksCountPerProject(1);
            var statusCodeResult = result as StatusCodeResult;

            Assert.IsNotNull(statusCodeResult);
            Assert.IsTrue(statusCodeResult.StatusCode == 404);
        }

        [TestMethod]
        public async Task GetUserTasksByID_HappyFlow()
        {
            var projectTaskRepoMock = new Mock<IRepository<ProjectTask>>();

            projectTaskRepoMock
                .Setup(m => m.ReadAll())
                .ReturnsAsync(new List<ProjectTask> { new ProjectTask { id = 0, name = "Test", performerId = 0 } });
            var userRepoMock = new Mock<IRepository<User>>();

            var sut = new ComplexQueryController(projectTaskRepoMock.Object, userRepoMock.Object);

            var result = await sut.GetUserTasksByID(0);
            var okResult = result as OkObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult.StatusCode == 200);

            var tasks = okResult.Value as List<ProjectTask>;
            Assert.IsTrue(tasks.Count == 1);
            Assert.AreEqual(tasks.First().id, 0);
            Assert.AreEqual(tasks.First().name, "Test");
            Assert.AreEqual(tasks.First().performerId, 0);
        }

        [TestMethod]
        public async Task GetUserTasksByID_NotFound()
        {
            var projectTaskRepoMock = new Mock<IRepository<ProjectTask>>();

            projectTaskRepoMock
                .Setup(m => m.ReadAll())
                .ReturnsAsync(new List<ProjectTask> { new ProjectTask { id = 0, name = "Test", performerId = 0 } });
            var userRepoMock = new Mock<IRepository<User>>();

            var sut = new ComplexQueryController(projectTaskRepoMock.Object, userRepoMock.Object);

            var result = await sut.GetUserTasksByID(1);
            var statusCodeResult = result as StatusCodeResult;

            Assert.IsNotNull(statusCodeResult);
            Assert.IsTrue(statusCodeResult.StatusCode == 404);
        }

        [TestMethod]
        public async Task GetUserFinishedTasks_HappyFlow()
        {
            var projectTaskRepoMock = new Mock<IRepository<ProjectTask>>();

            var now = DateTime.Now;

            projectTaskRepoMock
                .Setup(m => m.ReadAll())
                .ReturnsAsync(new List<ProjectTask> { new ProjectTask { id = 0, createdAt = now.AddDays(-2), finishedAt = now.AddDays(-1), performerId = 0 } });
            var userRepoMock = new Mock<IRepository<User>>();

            var sut = new ComplexQueryController(projectTaskRepoMock.Object, userRepoMock.Object);

            var result = await sut.GetUserFinishedTasks(0);
            var okResult = result as OkObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult.StatusCode == 200);

            var tasks = okResult.Value as List<TaskIdToName>;
            Assert.IsTrue(tasks.Count == 1);
            Assert.IsTrue(tasks.First().Id == 0);
        }

        [TestMethod]
        public async Task GetUserFinishedTasks_NotFound()
        {
            var projectTaskRepoMock = new Mock<IRepository<ProjectTask>>();

            var now = DateTime.Now;

            projectTaskRepoMock
                .Setup(m => m.ReadAll())
                .ReturnsAsync(new List<ProjectTask> { new ProjectTask { id = 0, createdAt = now.AddDays(-2), finishedAt = now.AddDays(-1), performerId = 0 } });
            var userRepoMock = new Mock<IRepository<User>>();

            var sut = new ComplexQueryController(projectTaskRepoMock.Object, userRepoMock.Object);

            var result = await sut.GetUserFinishedTasks(1);
            var statusCodeResult = result as StatusCodeResult;

            Assert.IsNotNull(statusCodeResult);
            Assert.IsTrue(statusCodeResult.StatusCode == 404);
        }

        [TestMethod]
        public async Task GetSortedUsersWithTasks_HappyFlow()
        {
            var projectTaskRepoMock = new Mock<IRepository<ProjectTask>>();

            var now = DateTime.Now;

            projectTaskRepoMock
                .Setup(m => m.ReadAll())
                .ReturnsAsync(new List<ProjectTask> { new ProjectTask { id = 0, createdAt = now.AddDays(-2), finishedAt = now.AddDays(-1), performerId = 0, name = "Test" } });

            var userRepoMock = new Mock<IRepository<User>>();
            userRepoMock.Setup(m => m.Read(0)).ReturnsAsync(new User { id = 0 });

            var sut = new ComplexQueryController(projectTaskRepoMock.Object, userRepoMock.Object);

            var result = await sut.GetSortedUsersWithTasks();
            var okResult = result as OkObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult.StatusCode == 200);

            var tasks = okResult.Value as List<Task<UserToTasks>>;
            var results = tasks.Select(t => t.Result);
            Assert.IsTrue(tasks.Count == 1);
        }
    }
}
