﻿using HomeTask4.Controllers;
using HomeTask4.Storage;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System.Threading.Tasks;

namespace HomeTask.InegrationTests
{
    [TestClass]
    public class UsersControllerContext
    {
        [TestMethod]
        public async Task RemoveUser()
        {
            var builder = new DbContextOptionsBuilder<HomeTaskContext>().UseInMemoryDatabase("RemoveUser");
            var context = new HomeTaskContext(builder.Options);
            var repo = new Repository<User>(context);
            var sut = new UsersController(repo);
            var user = new User { id = 0 };
            await sut.Post(user);

            repo.Delete(user);

            var all = await repo.ReadAll();
            Assert.IsTrue(all.Count == 0);
        }
    }
}
