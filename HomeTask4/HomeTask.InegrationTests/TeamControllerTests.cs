﻿using HomeTask4.Controllers;
using HomeTask4.Storage;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System.Threading.Tasks;

namespace HomeTask.InegrationTests
{
    [TestClass]
    public class TeamControllerTests
    {
        [TestMethod]
        public async Task CreateTeam() 
        {
            var builder = new DbContextOptionsBuilder<HomeTaskContext>().UseInMemoryDatabase("CreateTeam");
            var context = new HomeTaskContext(builder.Options);
            var repo = new Repository<Team>(context);
            var sut = new TeamsController(repo);

            var team = new Team { id = 0 };
            var response = await sut.Post(team);

            Assert.AreEqual(response.StatusCode, 201);
            
            var fromDb = await repo.Read(team.id);
            Assert.AreEqual(fromDb.id, team.id);
        }
    }
}
