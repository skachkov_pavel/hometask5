using HomeTask4.Controllers;
using HomeTask4.Storage;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System.Threading.Tasks;

namespace HomeTask.InegrationTests
{

    [TestClass]
    public class ProjectTaskControllerTests
    {
        [TestMethod]
        public async Task DeleteTask()
        {
            var builder = new DbContextOptionsBuilder<HomeTaskContext>().UseInMemoryDatabase("CreateProject");
            var context = new HomeTaskContext(builder.Options);
            var repo = new Repository<ProjectTask>(context);
            var sut = new ProjectTasksController(repo);

            var projectTask = new ProjectTask{ id = 0 };
            await repo.Create(projectTask);

            var result = sut.Delete(projectTask);
            Assert.AreEqual(result.StatusCode, 200);
            var fromDb = await repo.Read(projectTask.id);
            Assert.IsNull(fromDb);
        }
    }

    [TestClass]
    public class ProjectControllerTests
    {
        [TestMethod]
        public async Task CreateProject()
        {
            var builder = new DbContextOptionsBuilder<HomeTaskContext>().UseInMemoryDatabase("CreateProject");
            var context = new HomeTaskContext(builder.Options);
            var repo = new Repository<Project>(context);
            var sut = new ProjectsController(repo);

            var project = new Project { id = 0 };
            var response = await sut.Post(project);
            var result = response as StatusCodeResult;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, 201);

            var fromDb = repo.Read(project.id);
            Assert.IsNotNull(fromDb);
        }
    }
}
