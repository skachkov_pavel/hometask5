﻿namespace Models
{
    public class TaskIdToName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
