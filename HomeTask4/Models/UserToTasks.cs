﻿using System.Collections.Generic;

namespace Models
{
    public class UserToTasks
    {
        public string UserName { get; set; }
        public List<ProjectTask> ProjectTasks { get; set; }
    }
}
