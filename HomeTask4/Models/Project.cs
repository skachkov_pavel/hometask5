﻿using System;

namespace Models
{

    public class Project : IEntity
    {
        public int id { get; set; }
        public int authorId { get; set; }
        public User author { get; set; }
        public int teamId { get; set; }
        public Team team { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime deadline { get; set; }
        public DateTime createdAt { get; set; }
    }
}
