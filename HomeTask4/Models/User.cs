﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class User : IEntity
    {
        public int id { get; set; }
        public int? teamId { get; set; }
        public Team team { get; set; }
        [Required]
        public string firstName { get; set; }
        [Required]
        public string lastName { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public DateTime registeredAt { get; set; }
        [Required]
        public DateTime birthDay { get; set; }
    }
}
