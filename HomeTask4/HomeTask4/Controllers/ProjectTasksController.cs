﻿using HomeTask4.Storage;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Threading.Tasks;

namespace HomeTask4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectTasksController : ControllerBase
    {
        private readonly IRepository<ProjectTask> _repository;

        public ProjectTasksController(IRepository<ProjectTask> repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Ok(await _repository.ReadAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            return Ok(await _repository.Read(id));
        }

        [HttpPost]
        public async Task<StatusCodeResult> Post([FromBody] ProjectTask projectTask)
        {
            await _repository.Create(projectTask);
            return new StatusCodeResult(201);
        }

        [HttpPut]
        public async Task<StatusCodeResult> Put([FromBody] ProjectTask projectTask)
        {
            await _repository.Update(projectTask);
            return Ok();
        }

        [HttpDelete]
        public StatusCodeResult Delete([FromBody] ProjectTask projectTask)
        {
            _repository.Delete(projectTask);
            return Ok();
        }
    }
}
