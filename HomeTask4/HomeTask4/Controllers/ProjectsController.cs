﻿using HomeTask4.Storage;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Threading.Tasks;

namespace HomeTask4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IRepository<Project> _repository;

        public ProjectsController(IRepository<Project> repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Ok(await _repository.ReadAll()); 
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            return Ok(await _repository.Read(id));
        }

        [HttpPost]
        public async Task<StatusCodeResult> Post([FromBody] Project project)
        {
            await _repository.Create(project);
            return new StatusCodeResult(201);
        }

        [HttpPut]
        public async Task<StatusCodeResult> Put([FromBody] Project project)
        {
            await _repository.Update(project);
            return Ok();
        }

        [HttpDelete]
        public StatusCodeResult Delete([FromBody] Project project)
        {
            _repository.Delete(project);
            return Ok();
        }
    }
}
