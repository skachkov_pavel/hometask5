﻿using HomeTask4.Storage;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Threading.Tasks;

namespace HomeTask4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IRepository<Team> _repository;

        public TeamsController(IRepository<Team> repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Ok(await _repository.ReadAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            return Ok(await _repository.Read(id));
        }

        [HttpPost]
        public async Task<StatusCodeResult> Post([FromBody] Team team)
        {
            await _repository.Create(team);
            return new StatusCodeResult(201);
        }

        [HttpPut]
        public async Task<StatusCodeResult> Put([FromBody] Team team)
        {
            await _repository.Update(team);
            return Ok();
        }

        [HttpDelete]
        public StatusCodeResult Delete([FromBody] Team team)
        {
            _repository.Delete(team);
            return Ok();
        }
    }
}
