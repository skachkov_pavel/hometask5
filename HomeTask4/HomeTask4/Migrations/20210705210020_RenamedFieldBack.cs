﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HomeTask4.Migrations
{
    public partial class RenamedFieldBack : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "neDeadline",
                table: "projects",
                newName: "deadline");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "deadline",
                table: "projects",
                newName: "neDeadline");
        }
    }
}
