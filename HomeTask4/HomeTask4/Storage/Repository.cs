﻿using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeTask4.Storage
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        private DbSet<T> _entities;
        private HomeTaskContext homeTaskContext;

        public Repository(HomeTaskContext context)
        {
            homeTaskContext = context ?? throw new ArgumentNullException(nameof(context));
            _entities = context.Set<T>();
        }
        public async Task Create(T entity)
        {
            await _entities.AddAsync(entity);
            await homeTaskContext.SaveChangesAsync();
        }

        public void Delete(T entity)
        {
            _entities.Remove(entity);
            homeTaskContext.SaveChanges();
        }

        public async Task<T> Read(int id)
        {
            return await _entities.FirstOrDefaultAsync(p => p.id == id);
        }

        public async Task<IReadOnlyList<T>> ReadAll()
        {
            var result = await _entities.AsNoTracking().ToListAsync();
            return result.AsReadOnly();
        }

        public async Task Update(T entity)
        {
            T project = await Read(entity.id);
            if (project != null)
                await homeTaskContext.SaveChangesAsync();
        }
    }
}


