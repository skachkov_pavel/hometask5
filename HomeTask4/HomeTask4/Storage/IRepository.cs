﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HomeTask4.Storage
{
    public interface IRepository<T> where T: IEntity
    {
        Task Create(T entity);
        Task<T> Read(int id);
        Task<IReadOnlyList<T>> ReadAll();
        Task Update(T entity);
        void Delete(T entity);
    }
}
